#--------------------
# opcache.ini.erb
#--------------------
# PHP の CLI 版に対してオペコード・キャッシュを有効にします。
default['php5.6']['php-opcache']['opcache.ini']['enable_cli'] = 1
# OPcache によって使用される共有メモリ・ストレージのサイズ。（MB単位）
default['php5.6']['php-opcache']['opcache.ini']['memory_consumption'] = 128
# インターン (intern) された文字列を格納するために使用されるメモリ量。（MB単位）
default['php5.6']['php-opcache']['opcache.ini']['interned_strings_buffer'] = 8
# OPcache ハッシュテーブルのキー（すなわちスクリプト）の最大数。
default['php5.6']['php-opcache']['opcache.ini']['max_accelerated_files'] = 4000
# 更新のためにスクリプトのタイムスタンプをチェックする頻度。（秒単位）
# 0にすると、OPcacheは、リクエストごとに更新をチェックします。
# ※開発モードの時は0にしないと、変更したコードの更新されず混乱する場合があるので注意。
default['php5.6']['php-opcache']['opcache.ini']['revalidate_freq'] = 0
# 有効にすると、それぞれに割り当てられたブロックを解放しない、高速シャットダウン・シーケンスが使用されます。
# しかし、リクエスト変数のすべてのセットをひとまとめに割当てを解除することは、Zend Engineのメモリ・マネージャに依存します。
default['php5.6']['php-opcache']['opcache.ini']['fast_shutdown'] = 1