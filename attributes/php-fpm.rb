#--------------------------------------------------------------------
# php-fpm.conf.erb
#--------------------------------------------------------------------
# error_logを記録する場所
default['php5.6']['php-fpm']['php-fpm.conf']['error_log'] = "/var/log/php-fpm/error.log"

# events.mechanismが使用する
# - select     (any POSIX os)
# - poll       (any POSIX os)
# - epoll      (linux >= 2.5.44)
# Default Value: not set (auto detection)
default['php5.6']['php-fpm']['php-fpm.conf']['events.mechanism'] = "epoll"


#--------------------------------------------------------------------
# www.conf.erb
#--------------------------------------------------------------------
# Unix Socket通信で通信を行う（処理がちょっと早いらしい）
#default['php5.6']['php-fpm']['www.conf']['listen'] = "127.0.0.1:9000"
default['php5.6']['php-fpm']['www.conf']['listen'] = "/var/run/php-fpm.sock"

# unix socketのパーミッションを設定する
default['php5.6']['php-fpm']['www.conf']['listen.owner'] = "nginx"
default['php5.6']['php-fpm']['www.conf']['listen.group'] = "nginx"
default['php5.6']['php-fpm']['www.conf']['listen.mode'] = "0660"

# ユーザーとグループ設定
default['php5.6']['php-fpm']['www.conf']['user'] = "nginx"
default['php5.6']['php-fpm']['www.conf']['group'] = "nginx"

# 許可クライアントを自身のみに。
default['php5.6']['php-fpm']['www.conf']['listen.allowed_clients'] = "127.0.0.1"