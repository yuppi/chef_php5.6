#--------------------
# php.ini.erb
#--------------------
# 応答ヘッダーにPHPのバージョン等を出力しない
default['php5.6']['default']['php.ini']['expose_php'] = "Off"
# ログレベル。本番環境ではE_ALLとする
default['php5.6']['default']['php.ini']['error_reporting'] = "E_ALL | E_STRICT"
# ブラウザでのエラー表示。本番環境では Off とする
default['php5.6']['default']['php.ini']['display_errors'] = "On"
# エラーをログに残す
default['php5.6']['default']['php.ini']['log_errors'] = "On"
# 攻撃の内容を詳しく知る為に 4KB 程に設定
default['php5.6']['default']['php.ini']['log_errors_max_len'] = 4096
# エラーログ出力先
default['php5.6']['default']['php.ini']['error_log'] = "/var/log/php_error.log"
# 文字エンコーディング
default['php5.6']['default']['php.ini']['default_charset'] = "UTF-8"

# デフォルト言語
default['php5.6']['default']['php.ini']['mbstring.language'] = "Japanese"
# 内部文字エンコーディング
default['php5.6']['default']['php.ini']['mbstring.internal_encoding'] = "UTF-8"
# HTTP入力文字エンコーディングのデフォルト
default['php5.6']['default']['php.ini']['mbstring.http_input'] = "auto"
# 文字エンコーディング検出順序のデフォルト
default['php5.6']['default']['php.ini']['mbstring.detect_order'] = "auto"


#タイムアウトまでの秒数。
default['php5.6']['default']['php.ini']['max_execution'] = 30
# phpが使用できるメモリ量
default['php5.6']['default']['php.ini']['memory_limit'] = "256M"
# ファイルアップロードに関する設定
default['php5.6']['default']['php.ini']['post_max_size'] = "256M"
default['php5.6']['default']['php.ini']['upload_max_filesize'] = "256M"
default['php5.6']['default']['php.ini']['max_file_uploads'] = 20
# ソケット
default['php5.6']['default']['php.ini']['default_socket_timeout'] = "60"
# タイムゾーン
default['php5.6']['default']['php.ini']['timezone'] = "Asia/Tokyo"

