#
# Cookbook Name:: php5.6
# Recipe:: php-mbstring
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#
#--------------------------------------------------------------------
# php-mbstringのインストール
#--------------------------------------------------------------------
package "php-mbstring" do
    action :install
    options "--enablerepo=epel --enablerepo=remi --enablerepo=remi-php56"
end
