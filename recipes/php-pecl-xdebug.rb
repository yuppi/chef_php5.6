#
# Cookbook Name:: php5.6
# Recipe:: php-pecl-xdebug
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#
#--------------------------------------------------------------------
# php-pecl-xdebugのインストール
#--------------------------------------------------------------------
package "php-pecl-xdebug" do
    action :install
    options "--enablerepo=epel --enablerepo=remi --enablerepo=remi-php56"
end

#---------------
# xdebug.iniの設定
#---------------
project_name = data_bag_item('global_setting', 'project')['name']

# Chef::Log.logger.info node[:network][:interfaces]
# ip = node[:network][:interfaces][:eth1][:addresses].detect{|k,v| v[:family] == "inet" }.first
# remote_ip = ip.gsub /\.\d+$/, '.1'
template "xdebug.ini" do
    path "/etc/php.d/15-xdebug.ini"
    source "xdebug.ini.erb"
    mode 0644
    variables({
      # "remote_host" => remote_ip,
      "project_name" => project_name
    })
    # notifies :restart, 'service[httpd]'
end

