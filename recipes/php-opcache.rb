#
# Cookbook Name:: php5.6
# Recipe:: php-opcache
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#
#--------------------------------------------------------------------
# php-opcacheのインストール
#--------------------------------------------------------------------
package "php-opcache" do
    action :install
    options "--enablerepo=epel --enablerepo=remi --enablerepo=remi-php56"
end

#---------------
# opcache.iniの設定
#---------------
template "opcache.ini" do
    path "/etc/php.d/10-opcache.ini"
    source "opcache.ini.erb"
    mode 0644
end