#
# Cookbook Name:: php5.6
# Recipe:: php-mysql
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#
#--------------------------------------------------------------------
# php-mcryptのインストール
#--------------------------------------------------------------------
package "php-mysql" do
    action :install
    options "--enablerepo=epel --enablerepo=remi --enablerepo=remi-php56"
end
