#
# Cookbook Name:: php5.6
# Recipe:: default
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#
#--------------------------------------------------------------------
# php5.6のインストール
#--------------------------------------------------------------------
package "php" do
    action :install
    options "--enablerepo=epel --enablerepo=remi --enablerepo=remi-php56"
end

#---------------
# php.iniの設定
#---------------
template "php.ini" do
    path "/etc/php.ini"
    source "php.ini.erb"
    mode 0644
end