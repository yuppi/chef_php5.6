#
# Cookbook Name:: php5.6
# Recipe:: php-fpm
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#
#--------------------------------------------------------------------
# php-fpmのインストール
#--------------------------------------------------------------------
package "php-fpm" do
    action :install
    options "--enablerepo=epel --enablerepo=remi --enablerepo=remi-php56"
end

#--------------------------------------------------------------------
# php-fpm.confの設定
#--------------------------------------------------------------------
template "php-fpm.conf" do
    path "/etc/php-fpm.conf"
    source "php-fpm.conf.erb"
    mode 0644
end

#--------------------------------------------------------------------
# www.confの設定
#--------------------------------------------------------------------
template "php-fpm-www.conf" do
    path "/etc/php-fpm.d/www.conf"
    source "php-fpm-www.conf.erb"
    mode 0644
end

#--------------------------------------------------------------------
# php-fpmサービスの設定
#--------------------------------------------------------------------
service "php-fpm" do
    action [ :enable, :start ]
    supports :status => true, :restart => true, :reload => true
end

