#
# Cookbook Name:: php5.6
# Recipe:: php-mcrypt
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#
#--------------------------------------------------------------------
# php-mcryptのインストール
#--------------------------------------------------------------------
package "php-mcrypt" do
    action :install
    options "--enablerepo=epel --enablerepo=remi --enablerepo=remi-php56"
end
